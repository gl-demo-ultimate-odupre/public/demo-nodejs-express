#!/bin/bash

# Requires the following variables:
# * PAT: 
#   * a Project Access Token with API scope and Owner role.
#   * a Personal Access Token with API scope.
# * AGE_THRESHOLD (optional): The maximum age, in days, of the pipelines to keep. All older pipelines will be deleted.

glab auth login --hostname "$CI_SERVER_HOST" --token "$PAT"

# Set default value to AGE_THRESHOLD to 90 days if unset
AGE_THRESHOLD=${AGE_THRESHOLD:-90}
AGE_THRESHOLD_IN_SECONDS=$(($AGE_THRESHOLD*24*60*60)) # 90 days

# Get the list of pipelines in the project
PIPELINES=$(glab api --method GET projects/"$CI_PROJECT_ID"/pipelines)

# Loop over each entry of the PIPELINES json output
echo "$PIPELINES" | jq -r '.[] | .id, .created_at' | while read -r id; read -r created_at; do
    CREATED_AT_TS=$(date -d "$created_at" +%s)
    NOW=$(date +%s)
    AGE=$((NOW - CREATED_AT_TS))

    if [ $AGE -gt $AGE_THRESHOLD_IN_SECONDS ]; then
        echo "Pipeline ID $id created at $created_at is older than threshold $AGE_THRESHOLD days, deleting..."
        # Delete using the glab API primitive
        glab api --method DELETE "projects/$CI_PROJECT_ID/pipelines/$id"
    else
        echo "Pipeline ID $id created at $created_at is not older than threshold $AGE_THRESHOLD days. Ignoring."
    fi
done

#!/bin/bash

# Requires the following variables:
# * PAT: 
#   * a Project Access Token with API scope and Owner role.
#   * a Personal Access Token with API scope.
# * AGE_THRESHOLD (optional): The maximum age, in days, of the pipelines to keep. All older pipelines will be deleted.

glab auth login --hostname "$CI_SERVER_HOST" --token "$PAT"

# Set default value to AGE_THRESHOLD to 90 days if unset
AGE_THRESHOLD=${AGE_THRESHOLD:-90}
AGE_THRESHOLD_IN_HOURS=$(($AGE_THRESHOLD*24)) # Days to hours

# Delete all pipelines older than $AGE_THRESHOLD days
glab ci delete --older-than "$AGE_THRESHOLD_IN_HOURS"h